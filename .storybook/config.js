import { configure } from '@storybook/react';

import '@storybook/addon-console';

// automatically import all files ending in *.stories.js
const req = require.context('../lib/js/stories', true, /.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
