// https://github.com/storybooks/storybook/issues/3346#issuecomment-425516669
module.exports = ({ config }) => ({
    ...config,
    module: {
        ...config.module,
        rules: [
            // Temp fix for issue: https://github.com/storybooks/storybook/issues/3346
            ...config.module.rules.filter(rule => !(
                (rule.use && rule.use.length && rule.use.find(({ loader }) => loader === 'babel-loader'))
            )),
            {
                test: /\.jsx?$/,
                include: require('path').resolve('./'),
                exclude: /(node_modules|dist)/,
                loader: 'babel-loader',
            },
        ],
    },
});