open Storybook.React;

let vertResizer = Css.(style([
    width(`px(3)),
    cursor(`ewResize),
    backgroundColor(`hex("aaa"))
]));

let horizResizer = Css.(style([
    height(`px(3)),
    cursor(`nsResize),
    backgroundColor(`hex("aaa"))
]));

let topClass = Css.(style([
    marginTop(`auto),
    marginBottom(`zero)
]));

let leftClass = Css.(style([
    marginLeft(`auto),
    marginRight(`zero),
    textAlign(`right)
]));

let onChange = (n) => Js.log2("change", Js.Float.toString(n));
let onDragFinished = (n) => Js.log2("dragFinished", Js.Float.toString(n));

storiesOf("ReactSplitPane", Node.Module.module_)
|> add("vertical", _ =>
    <ReactSplitPane resizerClassName=vertResizer onChange onDragFinished>
        <div className=leftClass key="a">(ReasonReact.string("one"))</div>
        <div key="b">(ReasonReact.string("two"))</div>
    </ReactSplitPane>
)
|> add("horizontal", _ =>
    <ReactSplitPane resizerClassName=horizResizer onChange onDragFinished split=`Horizontal>
        <div className=topClass key="a">(ReasonReact.string("one"))</div>
        <div key="b">(ReasonReact.string("two"))</div>
    </ReactSplitPane>
);
