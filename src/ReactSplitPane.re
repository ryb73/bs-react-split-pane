open Belt.Option;

[@bs.module "react-split-pane"] [@react.component]
external make:
    (   ~defaultSize: Js.Json.t=?,
        ~maxSize: float=?,
        ~minSize: Js.Json.t=?,
        ~onChange: Js.Json.t => unit=?,
        ~onDragFinished: Js.Json.t => unit=?,
        ~paneClassName: string=?,
        ~paneStyle: ReactDOMRe.style=?,
        ~pane1ClassName: string=?,
        ~pane1Style: ReactDOMRe.style=?,
        ~pane2ClassName: string=?,
        ~pane2Style: ReactDOMRe.style=?,
        ~primary: string=?,
        ~resizerClassName: string=?,
        ~resizerStyle: ReactDOMRe.style=?,
        ~step: float=?,
        ~split: string=?,
        ~children: React.element,
    ) => React.element = "default";

let encodePrimary = fun
    | `First => "first"
    | `Second => "second";

let encodeSplit = fun
    | `Horizontal => "horizontal"
    | `Vertical => "vertical";

let encodeSize = fun
    | `Px(n) => Js.Json.number(n)
    | `Pct(n) => Js.Float.toString(n) ++ "%" |> Js.Json.string;

let wrapEventHandler = (f) =>
    (json) =>
        Js.Json.decodeNumber(json)
        |> Belt.Option.getExn
        |> f;

let makeProps =
    (   ~defaultSize=?, ~maxSize=?, ~minSize=?, ~paneStyle=?, ~pane1Style=?,
        ~pane2Style=?, ~primary=?, ~resizerStyle=?, ~step=?, ~split=?,
        ~paneClassName=?, ~pane1ClassName=?, ~pane2ClassName=?,
        ~resizerClassName=?, ~onChange=?, ~onDragFinished=?, ~children)
=>
    makeProps(
        ~primary=?map(primary, encodePrimary),
        ~split=?map(split, encodeSplit),
        ~defaultSize=?map(defaultSize, encodeSize),
        ~minSize=?map(minSize, encodeSize),
        ~onChange=?map(onChange, wrapEventHandler),
        ~onDragFinished=?map(onDragFinished, wrapEventHandler),
        ~step?, ~maxSize?, ~paneStyle?, ~pane1Style?, ~pane2Style?, ~resizerStyle?,
        ~paneClassName?, ~pane1ClassName?, ~pane2ClassName?, ~resizerClassName?,
        ~children,
    );
