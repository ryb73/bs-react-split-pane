let make: {.
        "children": React.element, "defaultSize": option(Js.Json.t),
        "maxSize": option(float), "minSize": option(Js.Json.t),
        "onChange": option(Js.Json.t => unit),
        "onDragFinished": option(Js.Json.t => unit),
        "pane1ClassName": option(string),
        "pane1Style": option(ReactDOMRe.style),
        "pane2ClassName": option(string),
        "pane2Style": option(ReactDOMRe.style),
        "paneClassName": option(string),
        "paneStyle": option(ReactDOMRe.style),
        "primary": option(string), "resizerClassName": option(string),
        "resizerStyle": option(ReactDOMRe.style),
        "split": option(string), "step": option(float)
    } => React.element;
let makeProps:
    (   ~defaultSize: [< `Pct(float) | `Px(float) ]=?,
        ~maxSize: float=?, ~minSize: [< `Pct(float) | `Px(float) ]=?,
        ~paneStyle: ReactDOMRe.style=?, ~pane1Style: ReactDOMRe.style=?,
        ~pane2Style: ReactDOMRe.style=?,
        ~primary: [< `First | `Second ]=?,
        ~resizerStyle: ReactDOMRe.style=?, ~step: float=?,
        ~split: [< `Horizontal | `Vertical ]=?, ~paneClassName: string=?,
        ~pane1ClassName: string=?, ~pane2ClassName: string=?,
        ~resizerClassName: string=?, ~onChange: float => unit=?,
        ~onDragFinished: float => unit=?, ~children: React.element,
        ~key: string=?, unit
    ) => {.
        "children": React.element, "defaultSize": option(Js.Json.t),
        "maxSize": option(float), "minSize": option(Js.Json.t),
        "onChange": option(Js.Json.t => unit),
        "onDragFinished": option(Js.Json.t => unit),
        "pane1ClassName": option(string),
        "pane1Style": option(ReactDOMRe.style),
        "pane2ClassName": option(string),
        "pane2Style": option(ReactDOMRe.style),
        "paneClassName": option(string),
        "paneStyle": option(ReactDOMRe.style),
        "primary": option(string), "resizerClassName": option(string),
        "resizerStyle": option(ReactDOMRe.style),
        "split": option(string), "step": option(float)
    };
